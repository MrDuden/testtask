﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace TestTask
{
    [CreateAssetMenu(fileName = "MapConfig", menuName = "MapConfig", order = 1)]
    public class MapConfig : ScriptableObject
    {
        public int mapOffset;
    }
}
