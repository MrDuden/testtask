﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace TestTask
{
    [RequireComponent(typeof(LineRenderer))]
    public class LineDrawer : MonoBehaviour
    {
        public int linesCount
        {
            get
            {
                return _linesCount;
            }
        }

        private LineRenderer _lineRenderer;
        private int _linesCount = 0;

        void Awake()
        {
            _lineRenderer = GetComponent<LineRenderer>();
        }

        /// <summary>
        /// Draw new line
        /// </summary>
        /// <param name="end"></param>
        public void DrawLine(Vector3 end)
        {
            _lineRenderer.positionCount = _linesCount + 1;
            _lineRenderer.SetPosition(_linesCount, end);

            _linesCount++;
        }

        /// <summary>
        /// Clear all lines
        /// </summary>
        public void Clear()
        {
            _lineRenderer.positionCount = 0;
            _linesCount = 0;
        }
    }
}