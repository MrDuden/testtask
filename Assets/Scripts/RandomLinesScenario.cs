﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace TestTask {
    public class RandomLinesScenario : MonoBehaviour
    {
        public MapCamera camera;
        public LineDrawer lineDrawer;
        public float lineDrawOffset;
        public int linesAmount;

        void Start()
        {
            camera.SetCameraOriginPoint(Vector3.zero);

            DrawLines();
        }

        void Update()
        {
            if (Input.GetKeyDown(KeyCode.Space))
            {
                DrawLines();
            }
        }

        private void DrawLines()
        {
            lineDrawer.Clear();

            for (int i = 0; i < linesAmount; i++)
            {
                var point = new Vector3(Random.Range(-lineDrawOffset, lineDrawOffset), Random.Range(-lineDrawOffset, lineDrawOffset), Random.Range(-lineDrawOffset, lineDrawOffset));
                lineDrawer.DrawLine(point);
            }

            camera.SetCameraOriginPoint(Vector3.zero);
        }
    }
}
