﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace TestTask
{
    public class MapCamera : MonoBehaviour
    {
        public OrbitCameraController cameraController;

        /// <summary>
        /// Set camera origin point on the map
        /// </summary>
        /// <param name="position"></param>
        public void SetCameraOriginPoint(Vector3 position)
        {
            cameraController.target = new Vector3(position.x, 0, position.z);


        }
    }
}
