﻿using System.Collections;
using System.Linq;
using System.Collections.Generic;
using UnityEngine;

namespace TestTask
{
    public class RouteDrawer : MonoBehaviour
    {
        public MapConfig mapConfig;

        [SerializeField]
        private LineDrawer _lineDrawer = null;
        private Route _route = null;
        private List<Vector3> _ucsPoints = null;
  
        /// <summary>
        /// Draw route in unity world space
        /// </summary>
        /// <param name="route"></param>
        public void DrawRoute(Route route)
        {
            _lineDrawer.Clear();
            _route = route;
            _ucsPoints = new List<Vector3>();

            foreach (var gpsPoint in route.points)
            {
                _ucsPoints.Add(CoordinatesToUCS(gpsPoint));
            }

            foreach (var point in _ucsPoints)
            {
                _lineDrawer.DrawLine(point);
            }
        }

        /// <summary>
        /// Calculate route origin point in unity coordinate system
        /// </summary>
        public Vector3 GetRouteOrigin()
        {
            var x = _ucsPoints.Sum(point => point.x) / _ucsPoints.Count;
            var y = _ucsPoints.Sum(point => point.y) / _ucsPoints.Count;
            var z = _ucsPoints.Sum(point => point.z) / _ucsPoints.Count;

            return new Vector3(x, y, z);
        }

        /// <summary>
        /// Convert GPS coordinates to unity coordinate system
        /// </summary>
        /// <param name="coords"></param>
        /// <returns></returns>
        private Vector3 CoordinatesToUCS(Vector3 coords)
        {
            var point = new Vector3();

            point.x = mapConfig.mapOffset * (180 + coords.x) / 360;
            point.y = 0;
            point.z = mapConfig.mapOffset * (90 - coords.y) / 180;

            return point;
        }
    }

    public class Route
    {
        public readonly List<Vector3> points = null;

        public Route(List<Vector3> points)
        {
            this.points = points;
        }
    }
}




