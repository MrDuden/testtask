﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace TestTask
{
    public class TestScenario : MonoBehaviour
    {
        public RouteDrawer routeDrawer;
        public MapCamera mapCamera;

        void Start()
        {
            var route = new Route(new List<Vector3>() { new Vector3(52.520008f, 13.404954f, 0) // Berlin
                , new Vector3(40.730610f, -73.935242f, 0)  // New York
                , new Vector3(-33.925840f, 18.423220f, 0) // Cape Town
                , new Vector3(52.520008f, 13.404954f, 0) }); // Berlin

            routeDrawer.DrawRoute(route);
            mapCamera.SetCameraOriginPoint(routeDrawer.GetRouteOrigin());
        }
    }
}
